NOTE: This project is a WIP and a proof of concept. The code is extremely unorganized and ugly, yet functional.

# Zaxord

If Discord met Dolby Axon.

This project convert's Discord's 1D voice channels into a 2D grid with spatial sound mapping. Pretend you're actually in a room chatting with your friends!

Note that this currently requires you to use your user key to log in.

# Known issues
- Discord kicks out the user every few minutes.

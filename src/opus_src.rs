use ambisonic::rodio::Source;
use audiopus::coder::Decoder;
use audiopus::{Channels, SampleRate};
use std::collections::VecDeque;
use std::sync::mpsc::{Receiver, TryRecvError};
use std::time::Duration;

// khz_to_ms*ms_frame_size*chans
pub const OUT_SIZE: usize = 48 * 20 * 2;
const MONO_OUT: usize = OUT_SIZE / 2;

pub struct OpusStream {
    decoder: Decoder,
    buf: VecDeque<f32>,
    rx: Receiver<Vec<u8>>,
}

impl OpusStream {
    pub fn new(rx: Receiver<Vec<u8>>) -> Self {
        Self {
            decoder: Decoder::new(SampleRate::Hz48000, Channels::Stereo).expect("dec"),
            buf: VecDeque::with_capacity(1024),
            rx,
        }
    }

    fn insert(&mut self, packet: Vec<u8>) {
        let mut result = vec![0.; OUT_SIZE];
        self.decoder
            .decode_float(Some(&packet), &mut result, false)
            .expect("df");

        for (i, val) in result.iter().enumerate().step_by(2) {
            self.buf.push_back((val * 0.3) + (result[i + 1] * 0.3));
        }
    }
}

impl Iterator for OpusStream {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        match self.rx.try_recv() {
            Ok(v) => {
                //println!("New");
                self.insert(v);
            }
            Err(TryRecvError::Empty) => {
                // sorry nothing
            }
            Err(e) => {
                panic!("opuser {:?}", e);
            }
        }
        self.buf.pop_front().or(Some(0.0))
    }
}

impl Source for OpusStream {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn sample_rate(&self) -> u32 {
        48000
    }

    fn total_duration(&self) -> Option<Duration> {
        None
    }
}

use crate::audioer::Audioer;
use crate::discord::DiscordWebsock;
use iced::{Application, Settings};

mod audioer;
mod discord;
mod opus_src;
mod ui;

#[tokio::main]
async fn main() {
    let (mut sock, guild_rx, action_tx, audio_tx) = DiscordWebsock::new();
    let discord = tokio::spawn(async move { sock.run().await });
    let audio = std::thread::spawn(|| {
        Audioer::new(audio_tx).run();
        println!("dead aud")
    });
    ui::Ui::run(Settings::with_flags((guild_rx, action_tx)));
    audio.join();
}

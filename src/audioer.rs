use crate::opus_src::OpusStream;
use ambisonic::{Ambisonic, AmbisonicBuilder, SoundController};
use std::collections::HashMap;
use std::sync::mpsc::{channel, Sender};
use tokio::sync::mpsc::error::TryRecvError;
use tokio::sync::mpsc::Receiver;

#[derive(Debug, Clone)]
pub enum AudioAction {
    Join(String, (i16, i16)),
    Opus(String, Vec<u8>),
    /// Relative x and y
    Moved(String, bool, (u16, u16), (i16, i16)),
    // TODO: Disconn
}

pub struct Audioer {
    streams: HashMap<String, Sender<Vec<u8>>>,
    handles: HashMap<String, SoundController>,
    audio_rx: Receiver<AudioAction>,
    scene: Ambisonic,
    my_id: String,
    pos: (u16, u16),
}

impl Audioer {
    pub fn new(audio_rx: Receiver<AudioAction>) -> Self {
        Self {
            streams: HashMap::new(),
            handles: HashMap::new(),
            scene: AmbisonicBuilder::default().build(),
            audio_rx,
            my_id: "".to_string(),
            pos: (0, 0),
        }
    }

    /// Run forever
    pub fn run(&mut self) {
        loop {
            match self.audio_rx.try_recv() {
                Ok(act) => {
                    match act {
                        AudioAction::Opus(user, data) => {
                            if let Some(sender) = self.streams.get(&user) {
                                sender.send(data).expect("opsnd");
                            } else {
                                let (tx, rx) = channel();
                                let tx = self.streams.entry(user.clone()).or_insert(tx);
                                let pos = calc_pos(-(self.pos.0 as i16), -(self.pos.1 as i16));
                                self.handles.insert(
                                    user,
                                    self.scene
                                        .play_at(OpusStream::new(rx), pos),
                                );
                                tx.send(data);
                            }
                        }
                        AudioAction::Moved(user, us, pos, (relx, rely)) => {
                            if us {
                                self.pos = pos;
                            }
                            let pos = calc_pos(relx, rely);
                            println!("He movin {:?}", &pos);
                            if let Some(handle) = self.handles.get_mut(&user) {
                                handle.adjust_position(pos);
                            } else {
                                let (tx, rx) = channel();
                                self.streams.insert(user.clone(), tx);
                                self.handles
                                    .insert(user, self.scene.play_at(OpusStream::new(rx), pos));
                            }
                        }
                        AudioAction::Join(my_id, (my_x, my_y)) => {
                            self.my_id = my_id;
                        }
                    }
                }
                Err(TryRecvError::Empty) => {
                    // sorry nothing
                }
                Err(e) => {}
            }
        }
    }
}

fn calc_pos(relx: i16, rely: i16) -> [f32;3] {
    let xf = relx as f32 * 2.0; // TODO: I dunno bout z
    let yf = -rely as f32 * 2.0;
    [
        if xf == 0.0 { 0.01 } else { xf },
        if yf == 0.0 { 0.01 } else { yf / 10.0 },
        if yf == 0.0 { 0.01 } else { yf },
    ]
}

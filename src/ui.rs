use crate::discord::{DiscordAction, Guild, User};
use ::image::DynamicImage;
use ::image::GenericImageView;
use ::image::ImageFormat;
use iced::canvas::event::Status;
use iced::canvas::{Cursor, Event, Frame, Geometry, Path, Program, Stroke};
use iced::*;
use lazy_static::lazy_static;
use std::collections::{HashMap, HashSet};
use std::io::{BufReader, Read};
use std::sync::Arc;
use std::sync::Mutex as stdMutex;
use tokio::sync::mpsc::{Receiver, Sender};
use tokio::sync::Mutex;

lazy_static! {
    static ref MASK: DynamicImage =
        ::image::load_from_memory_with_format(include_bytes!("av-mask.png"), ImageFormat::Png)
            .expect("aaa");
}

const ALPHA_RESOLUTION: f32 = 1.0 / 255.0;

/// Inserts an image at pos on the canvas, masked with mask. This might be super nasty slow
pub fn img_cut_path(
    image: &DynamicImage,
    mask: &DynamicImage,
    pos: (f32, f32),
) -> Vec<(Path, Color)> {
    let mut ret = vec![];
    let (width, height) = image.dimensions();
    let (mwidth, mheight) = mask.dimensions();

    let wratio = mwidth as f64 / width as f64;
    let hratio = mheight as f64 / height as f64;
    //println!("start");

    for x in 0..width {
        for y in 0..height {
            let px = image.get_pixel(x, y);
            let alpha = mask.get_pixel((x as f64 * wratio) as u32, (y as f64 * hratio) as u32);
            let new_a =
                255 - ((alpha.0[0] as u32 + alpha.0[1] as u32 + alpha.0[2] as u32) / 3) as u8;

            if new_a > 0 {
                ret.push((
                    Path::rectangle(
                        Point::new(pos.0 + x as f32, pos.1 + y as f32),
                        Size::new(1.0, 1.0),
                    ),
                    Color::from_rgba8(px.0[0], px.0[1], px.0[2], new_a as f32 * ALPHA_RESOLUTION),
                ));
            }
        }
    }
    //println!("done");
    ret
}

const W: u16 = 56;
const H: u16 = 56;
const GRID: u16 = 10;
const GRID_SIZE: u16 = W * GRID + 2;
const AV_SIZE: u16 = 40;
const AV_CENTER_OFFSET: f32 = (H - AV_SIZE) as f32 / 2.0;

struct GridState {
    mouse_down: (bool, bool),
    moving_grid_update: (u16, u16),
    dragging: Option<((u16, u16), Option<String>)>,
    discord_tx: Sender<DiscordAction>,
}

// stdMutex is used because draw doesn't have mutability TODO
struct Grid<'a> {
    users: stdMutex<&'a mut HashMap<String, User>>,
    // id, imgdata
    img_cache: stdMutex<&'a mut HashMap<String, Geometry>>,
    // id, imgdata
    talk_circ_cache: &'a HashMap<(u16, u16), Geometry>,
    // gid, vid
    current_preview: &'a (String, String, String),
    // gid, vid
    in_voice: &'a mut Option<(String, String)>,
    my_user: &'a mut Option<User>,
    grid_state: &'a mut GridState,
    talking: &'a HashSet<String>,
}

impl<'a> Grid<'a> {
    fn pos_to_grid(&self, pos: Point) -> (u16, u16) {
        (
            (pos.x / W as f32).min((GRID - 1) as f32).max(0.0) as u16,
            (pos.y / H as f32).min((GRID - 1) as f32).max(0.0) as u16,
        )
    }

    fn send_new_pos(&self, vgid: String, vvid: String, uid: String, us: bool, pos: (u16, u16)) {
        println!("Send new pos");
        let tx = self.grid_state.discord_tx.clone();
        let my_pos = self.my_user.as_ref().unwrap().pos.unwrap().clone();
        println!("Ours {:?} the {:?}", my_pos, pos);
        tokio::spawn(async move {
            // TODO: Not okay?
            tx.send(DiscordAction::NewPos((
                vgid,
                vvid,
                uid,
                us,
                pos.0,
                pos.1,
                pos.0 as i16 - my_pos.0 as i16,
                pos.1 as i16 - my_pos.1 as i16,
            )))
            .await
            .ok();
        });
    }
}

impl<'a> Program<Message> for Grid<'a> {
    fn update(
        &mut self,
        event: Event,
        bounds: Rectangle,
        cursor: Cursor,
    ) -> (Status, Option<Message>) {
        //println!("UPD");
        let mut message = None;
        match event {
            Event::Mouse(evt) => {
                match evt {
                    mouse::Event::CursorEntered => {
                        self.grid_state.mouse_down = (false, false);
                        self.grid_state.dragging = None;
                    }
                    mouse::Event::CursorLeft => {
                        self.grid_state.mouse_down = (false, false);
                        self.grid_state.dragging = None;
                    }
                    mouse::Event::CursorMoved { .. } => {
                        // TODO: If not using client
                        if let Some((vgid, vvid)) = self.in_voice.as_ref() {
                            if &self.current_preview.0 == vgid && &self.current_preview.1 == vvid {
                                if let Some(pos) = cursor.position_in(&bounds) {
                                    // Only if we're dragginig something
                                    if let Some((_, Some(id))) = &self.grid_state.dragging {
                                        let pos = self.pos_to_grid(pos);
                                        if pos != self.grid_state.moving_grid_update {
                                            let mut users = self.users.lock().unwrap();
                                            let my_id = if let Some(me) = self.my_user {
                                                if id == &me.id {
                                                    me.pos = Some(pos.clone());
                                                }
                                                me.id.clone()
                                            } else {
                                                unreachable!();
                                            };
                                            if let Some((_, user)) =
                                                users.iter_mut().find(|(uid, _)| uid == &id)
                                            {
                                                user.pos = Some(pos);
                                                println!("Someone bein moved {:?}", user.pos);
                                                let mut lock = self.img_cache.lock().unwrap();
                                                lock.remove(id);
                                            }
                                            if id == &my_id {
                                                for (uid, user) in users.iter() {
                                                    self.send_new_pos(
                                                        vgid.clone(),
                                                        vvid.clone(),
                                                        uid.clone(),
                                                        uid == &my_id,
                                                        user.pos.unwrap_or((0, 0)).clone(),
                                                    );
                                                }
                                            } else {
                                                self.send_new_pos(
                                                    vgid.clone(),
                                                    vvid.clone(),
                                                    id.clone(),
                                                    false,
                                                    pos.clone(),
                                                );
                                            }
                                            self.grid_state.moving_grid_update = pos;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    mouse::Event::ButtonPressed(btn) => match btn {
                        mouse::Button::Left => {
                            self.grid_state.mouse_down.0 = true;
                            if let Some(pos) = cursor.position_in(&bounds) {
                                let pos = self.pos_to_grid(pos);
                                let users = self.users.lock().unwrap();
                                if let Some((uid, _)) =
                                    users.iter().find(|(_, u)| u.pos.unwrap_or((0, 0)) == pos)
                                {
                                    self.grid_state.dragging = Some((pos, Some(uid.clone())));
                                } else {
                                    self.grid_state.dragging = Some((pos, None));
                                }
                            }
                        }
                        mouse::Button::Right => {
                            self.grid_state.mouse_down.1 = true;
                        }
                        _ => {}
                    },
                    mouse::Event::ButtonReleased(btn) => match btn {
                        mouse::Button::Left => {
                            let can_join = if let Some((vgid, vvid)) = self.in_voice {
                                !(&self.current_preview.0 == vgid
                                    && &self.current_preview.1 == vvid)
                            } else {
                                true
                            };
                            if can_join && self.grid_state.mouse_down.0 {
                                if let Some((_, None)) = self.grid_state.dragging {
                                    if let Some(pos) = cursor.position_in(&bounds) {
                                        let pos = self.pos_to_grid(pos);
                                        let mut users = self.users.lock().unwrap();
                                        let new_voice = (
                                            self.current_preview.0.clone(),
                                            self.current_preview.1.clone(),
                                        );
                                        let my_id = if let Some(me) = self.my_user {
                                            me.pos = Some(pos.clone());
                                            users.insert(me.id.clone(), me.clone());
                                            me.id.clone()
                                        } else {
                                            unreachable!();
                                        };
                                        for (uid, user) in users.iter() {
                                            self.send_new_pos(
                                                new_voice.0.clone(),
                                                new_voice.1.clone(),
                                                uid.clone(),
                                                uid == &my_id,
                                                user.pos.unwrap_or((0, 0)),
                                            );
                                        }
                                        *self.in_voice = Some(new_voice.clone());
                                        message = Some(Message::VoiceConnect(new_voice));
                                    }
                                }
                            }
                            self.grid_state.mouse_down.0 = false;
                            self.grid_state.dragging = None;
                        }
                        mouse::Button::Right => {
                            self.grid_state.mouse_down.1 = false;
                        }
                        _ => {}
                    },
                    mouse::Event::WheelScrolled { .. } => {}
                }
            }
            Event::Keyboard(_) => {}
        }
        // An event tells the window something happened
        (Status::Ignored, message)
    }

    fn draw(&self, bounds: Rectangle, _: Cursor) -> Vec<Geometry> {
        //println!("DRAW");
        let mut geometries = vec![];
        let mut users = self.users.lock().unwrap();

        let mut frame = Frame::new(bounds.size());

        for i in 0..GRID {
            for j in 0..GRID {
                let rect_size = ((i * W + 1) as f32, (j * H + 1) as f32);
                let sq = Path::rectangle(
                    Point::new(rect_size.0, rect_size.1),
                    Size::new(W as f32, H as f32),
                );
                frame.stroke(
                    &sq,
                    Stroke::default().with_color(Color::from_rgb8(5, 172, 98)),
                );
                for (uid, u) in users.iter_mut() {
                    let upos = u.pos.unwrap_or((0, 0));
                    if upos.0 == i && upos.1 == j {
                        if self.talking.contains(uid) {
                            geometries.push(self.talk_circ_cache.get(&upos).expect("No talk circ").clone());
                        }
                        let mut lock = self.img_cache.lock().unwrap();
                        if let Some(img) = lock.get(uid) {
                            //println!("MAPPING");
                            geometries.push(img.clone());
                        } else if let Some(img) = &u.av_img {
                            //println!("LOADING");
                            let img = img_cut_path(
                                img,
                                &MASK,
                                (
                                    rect_size.0 + AV_CENTER_OFFSET,
                                    rect_size.1 + AV_CENTER_OFFSET,
                                ),
                            );
                            let mut frame = Frame::new(bounds.size());
                            for (path, color) in &img {
                                frame.fill(path, *color);
                            }
                            let new_geom = frame.into_geometry();
                            lock.insert(uid.clone(), new_geom.clone());
                            geometries.push(new_geom);
                        } else {
                            //println!("Getting image...");
                            if let Some(avatar) = &u.avatar {
                                // TODO: Thread this, discord get is slow
                                if let Ok(av) = blocking_reqwest::get(&format!(
                                    "https://cdn.discordapp.com/avatars/{}/{}.png?size=40",
                                    uid, avatar
                                )) {
                                    u.av_img = ::image::load(
                                        BufReader::new(std::io::Cursor::new(
                                            av.bytes()
                                                .map(|b| b.expect("Bad bytes"))
                                                .collect::<Vec<_>>(),
                                        )),
                                        ImageFormat::Png,
                                    )
                                    .ok();
                                    println!("oimg {:?}", u.av_img);
                                    if let Some(img) = &u.av_img {
                                        let img = img_cut_path(
                                            img,
                                            &MASK,
                                            (
                                                rect_size.0 + AV_CENTER_OFFSET,
                                                rect_size.1 + AV_CENTER_OFFSET,
                                            ),
                                        );
                                        let mut frame = Frame::new(bounds.size());
                                        for (path, color) in &img {
                                            frame.fill(path, *color);
                                        }
                                        let new_geom = frame.into_geometry();
                                        lock.insert(uid.clone(), new_geom.clone());
                                        geometries.push(new_geom);
                                    }
                                    // TODO: Some other thing on err
                                }
                            } // TODO if no avatar
                        }
                    }
                }
                //println!("sdsd {:?}", users);
            }
        }
        geometries.push(frame.into_geometry());
        geometries
    }
}

#[derive(Debug, Clone)]
pub enum UiAction {
    Relog,
    Guilds(HashMap<String, Guild>),
    MyUser(Option<User>),
    Connected,
    Disconnected,
    Speaking(bool, String),
}

pub struct UiState {
    scroll: scrollable::State,
    input: text_input::State,
    search: text_input::State,
    discon_btn: button::State,
    mute_btn: button::State,
    deaf_btn: button::State,
    grid_state: GridState,
    voice_users: HashMap<String, User>,
    av_cache: HashMap<String, Geometry>,
    talk_circ_cache: HashMap<(u16, u16), Geometry>,
    // gid, vid, name
    current_voice: (String, String, String),
    // gid, vid
    in_voice: Option<(String, String)>,
    user_key_value_hidden: String,
    user_key_value_real: String,
    search_value: String,
    ui_rx: Arc<Mutex<Receiver<UiAction>>>,
    discord_tx: Sender<DiscordAction>,
    guilds: Option<HashMap<String, Guild>>,
    search_ids: Vec<String>,
    my_user: Option<User>,
    talking: HashSet<String>,
    muted: bool,
}

pub enum Ui {
    UserKey(Option<UiState>),
    LoadingGuilds(Option<UiState>),
    Guilds(Option<UiState>),
}

#[derive(Debug, Clone)]
pub enum Message {
    InputChanged(String),
    SubmitUserKey,
    Action(UiAction),
    PreviewVoice((String, String)),
    Disconnect((String, String)),
    VoiceConnect((String, String)),
    Deaf((String, String)),
    Mute((String, String)),
}

impl Ui {
    /// Maps a channel into a Message event
    async fn wait_for_ui_action(ui_rx: Arc<Mutex<Receiver<UiAction>>>) -> UiAction {
        println!("Locking");
        let mut mutex = ui_rx.lock().await;
        println!("getting");
        mutex.recv().await.expect("Channel somehow closed")
    }
}

impl Application for Ui {
    type Executor = iced::executor::Default;
    type Message = Message;
    type Flags = (Receiver<UiAction>, Sender<DiscordAction>);

    fn new(flags: Self::Flags) -> (Self, Command<Self::Message>) {

        let mut talk_circ_cache = HashMap::with_capacity(100);

        // Draw all the possible green talking circles once
        for i in 0..GRID {
            for j in 0..GRID {
                let mut frame = Frame::new(Size::new(GRID_SIZE as f32, GRID_SIZE as f32));
                let rect_size = ((i * W + 1) as f32, (j * H + 1) as f32);
                let sq = 22.0;
                let half = W as f32 * 0.5;
                let circ = Path::circle(
                    Point::new(rect_size.0 + half, rect_size.1 + half),
                    sq,
                );
                frame.fill(&circ, Color::from_rgb8(5, 172, 98));
                talk_circ_cache.insert((i, j), frame.into_geometry());
            }
        }

        (
            Self::UserKey(Some(UiState {
                scroll: scrollable::State::new(),
                input: text_input::State::focused(),
                search: text_input::State::focused(),
                discon_btn: button::State::new(),
                mute_btn: button::State::new(),
                deaf_btn: button::State::new(),
                grid_state: GridState {
                    mouse_down: (false, false),
                    moving_grid_update: (0, 0),
                    dragging: None,
                    discord_tx: flags.1.clone(),
                },
                voice_users: HashMap::with_capacity(8),
                av_cache: HashMap::with_capacity(16),
                talk_circ_cache,
                current_voice: (
                    "".to_string(),
                    "".to_string(),
                    "Please Select a Channel".to_string(),
                ),
                in_voice: None,
                user_key_value_hidden: "".to_string(),
                user_key_value_real: "".to_string(),
                search_value: "".to_string(),
                ui_rx: Arc::new(Mutex::new(flags.0)),
                discord_tx: flags.1,
                guilds: None,
                search_ids: Vec::with_capacity(100),
                my_user: None,
                talking: HashSet::with_capacity(8),
                muted: false,
            })),
            Command::none(),
        )
    }

    fn title(&self) -> String {
        "Zaxord v0.3.0".to_string()
    }

    fn update(
        &mut self,
        mut message: Self::Message,
        _clipboard: &mut Clipboard,
    ) -> Command<Self::Message> {
        if let Message::Action(action) = &mut message {
            println!("ui act {:?}", action);
            match action {
                UiAction::Relog => {
                    // Start again
                    match self {
                        Ui::LoadingGuilds(state) | Ui::Guilds(state) => {
                            *self = Self::UserKey(state.take())
                        }
                        _ => {}
                    }
                }
                UiAction::Guilds(ref mut map) => {
                    match self {
                        Ui::LoadingGuilds(Some(state))
                        | Ui::Guilds(Some(state)) => {
                            for (gid, guild) in map.iter_mut() {
                                for (vid, voice) in guild.voices.iter_mut() {
                                    if gid == &state.current_voice.0
                                        && vid == &state.current_voice.1
                                    {
                                        for (uid, u) in state.voice_users.iter_mut() {
                                            if let Some(user) = voice.users.get_mut(uid) {
                                                user.av_img = u.av_img.take();
                                                user.pos = u.pos;
                                            } else {
                                                // user left
                                                state.av_cache.remove(uid);
                                            }
                                        }
                                        state.voice_users = voice.users.clone();
                                    } else {
                                        if let Some(guilds) = &mut state.guilds {
                                            let eguild = guilds.get_mut(gid).expect("No guild a");
                                            let evoice =
                                                eguild.voices.get_mut(vid).expect("No voice a");
                                            for (uid, u) in evoice.users.iter_mut() {
                                                if let Some(user) = voice.users.get_mut(uid) {
                                                    user.av_img = u.av_img.take();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            state.guilds = Some(map.to_owned());
                        }
                        _ => {}
                    }
                }
                UiAction::MyUser(id) => match self {
                    Ui::LoadingGuilds(Some(state))
                    | Ui::Guilds(Some(state)) => {
                        state.my_user = id.take();
                    }
                    _ => {}
                },
                UiAction::Connected => {}
                UiAction::Disconnected => match self {
                    Ui::LoadingGuilds(Some(state))
                    | Ui::Guilds(Some(state)) => {
                        state.voice_users.clear();
                        state.in_voice = None;
                        state.talking.clear();
                    }
                    _ => {}
                },
                UiAction::Speaking(speak, uid) => {
                    match self {
                        Ui::Guilds(Some(state)) => {
                            if *speak {
                                state.talking.insert(uid.clone());
                            } else {
                                state.talking.remove(uid);
                            }
                        }
                        _ => {}
                    }
                }
            }
        }
        match self {
            Ui::UserKey(state) => {
                match message {
                    Message::InputChanged(key) => {
                        if let Some(state) = state {
                            if key.len() > 0 || state.user_key_value_real.len() > 0 {
                                if key.len() < state.user_key_value_real.len() {
                                    state.user_key_value_real.truncate(key.len());
                                    state.user_key_value_hidden.truncate(key.len());
                                } else {
                                    //println!("KEY {:?}", key);
                                    let new = key.replace("*", "").replace("\"", "");
                                    state.user_key_value_real += &new;
                                    //println!("REAL {:?}", state.user_key_value_real);
                                    for _ in new.chars() {
                                        state.user_key_value_hidden += "*";
                                    }
                                }
                            }
                        }
                    }
                    Message::SubmitUserKey => {
                        let should = if let Some(state) = state {
                            state.user_key_value_real.len() > 0
                        } else {
                            unreachable!();
                        };

                        if should {
                            if let Some(state) = state {
                                let token = std::mem::replace(
                                    &mut state.user_key_value_real,
                                    String::new(),
                                );
                                let tx = state.discord_tx.clone();
                                tokio::spawn(async move {
                                    // TODO: Not okay?
                                    tx.send(DiscordAction::Token(token)).await.ok();
                                });
                                state.user_key_value_hidden.clear();
                            }
                            *self = Self::LoadingGuilds(state.take());
                        }
                    }
                    _ => {}
                }
            }
            Ui::LoadingGuilds(state) => {
                let should = if let Some(state) = state {
                    state.guilds.is_some()
                } else {
                    unreachable!();
                };

                if should {
                    *self = Self::Guilds(state.take());
                }
            }
            Ui::Guilds(state) => {
                match message {
                    Message::PreviewVoice((gid, vid)) => {
                        if let Some(state) = state {
                            if let Some(guilds) = &mut state.guilds {
                                if !(vid == state.current_voice.1 && gid == state.current_voice.0) {
                                    if let Some(guild) = guilds.get_mut(&state.current_voice.0) {
                                        if let Some(voice) =
                                            guild.voices.get_mut(&state.current_voice.1)
                                        {
                                            println!("Invalidating old users");
                                            // Move our captured state over
                                            for (id, u) in state.voice_users.iter_mut() {
                                                if let Some(mut user) = voice.users.get_mut(id) {
                                                    user.av_img = u.av_img.take();
                                                    user.pos = u.pos;
                                                    // TODO: Get new position?
                                                }
                                                state.av_cache.remove(id);
                                            }
                                        }
                                    }
                                    if let Some(guild) = guilds.get_mut(&gid) {
                                        if let Some(voice) = guild.voices.get_mut(&vid) {
                                            // Only when moving servers
                                            state.current_voice = (gid, vid, voice.name.clone());
                                            let new_users = voice.users.clone();
                                            /*let mut x = 0;
                                            let mut y = 0;
                                            for (_, u) in new_users.iter_mut() {
                                                // TODO: If user doesn't have pos from tcp
                                                if u.pos.is_none() {
                                                    u.pos = (x, y);
                                                    x += 1;
                                                    if x == 10 {
                                                        x = 0;
                                                        y += 1;
                                                        if y == 10 {
                                                            y = 0;
                                                        }
                                                    }
                                                }
                                            }*/
                                            state.voice_users = new_users;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Message::Disconnect(ids) => {
                        if let Some(state) = state {
                            if let Some(voice) = &state.in_voice {
                                if &ids == voice {
                                    let tx = state.discord_tx.clone();
                                    tokio::spawn(async move {
                                        // TODO: Not okay?
                                        tx.send(DiscordAction::Disconnect).await.ok();
                                    });
                                }
                            }
                        }
                    }
                    Message::Deaf(ids) => {
                        if let Some(state) = state {
                            if let Some(voice) = &state.in_voice {
                                if &ids == voice {
                                    let tx = state.discord_tx.clone();
                                    tokio::spawn(async move {
                                        // TODO: Not okay?
                                        tx.send(DiscordAction::Deaf(ids)).await.ok();
                                    });
                                }
                            }
                        }
                    }
                    Message::Mute(ids) => {
                        if let Some(state) = state {
                            if let Some(voice) = &state.in_voice {
                                if &ids == voice {
                                    state.muted = !state.muted;
                                    let tx = state.discord_tx.clone();
                                    tokio::spawn(async move {
                                        // TODO: Not okay?
                                        tx.send(DiscordAction::Mute(ids)).await.ok();
                                    });
                                }
                            }
                        }
                    }
                    Message::VoiceConnect(ids) => {
                        if let Some(state) = state {
                            let tx = state.discord_tx.clone();
                            tokio::spawn(async move {
                                // TODO: Not okay?
                                tx.send(DiscordAction::Connect(ids)).await.ok();
                            });
                        }
                    }
                    Message::InputChanged(search) => {
                        if let Some(state) = state {
                            println!("Search s {:?}", search);
                            let search_lower = search.to_lowercase();
                            state.search_ids.clear();
                            if search.len() > 0 {
                                if let Some(guilds) = &state.guilds {
                                    for (gid, guild) in guilds {
                                        if guild.name.to_lowercase().contains(&search_lower) {
                                            state.search_ids.push(gid.clone());
                                        }
                                    }
                                }
                            }
                            state.search_value = search;
                        }
                    }
                    _ => {}
                }
            }
            _ => {}
        }

        // get another action
        match self {
            Ui::UserKey(Some(state))
            | Ui::LoadingGuilds(Some(state))
            | Ui::Guilds(Some(state)) => {
                if state.ui_rx.try_lock().is_err() {
                    Command::none()
                } else {
                    Command::perform(
                        Self::wait_for_ui_action(state.ui_rx.clone()),
                        Message::Action,
                    )
                }
            }
            _ => {
                unreachable!()
            }
        }
    }

    fn view(&mut self) -> Element<'_, Self::Message> {
        println!("Aaaaa");
        Container::new::<Element<'_, Self::Message>>(match self {
            Ui::UserKey(Some(state)) => {
                state.guilds = None;
                let title = Text::new("Please Enter Your User Key")
                    .width(Length::Fill)
                    .size(50)
                    .horizontal_alignment(HorizontalAlignment::Center);

                let input = TextInput::new(
                    &mut state.input,
                    "Your User Key Here",
                    &*state.user_key_value_hidden,
                    Message::InputChanged,
                )
                .padding(15)
                .size(30)
                .on_submit(Message::SubmitUserKey)
                .style(style::Input);
                let howto = Text::new(
                    "\nHow to Get Your User Key:\n\n\
                1) Open Discord.\n\
                2) Press Ctrl+Shift+i together to open the Inspector tool.\n\
                3) Find the \"Application\" tab at the top.\n\
                4) Click the dropdown arrow next to \"Local Storage\" on the left.\n\
                5) Click the Discord item in that list.\n\
                6) Press Ctrl+r together.\n\
                7) Type \"token\" in the Filter box.\n\
                8) Double click the token's \"Value\" field and copy the text.\n\
                9) Close the Inspector tool with Ctrl+Shift+i again.",
                )
                .width(Length::Fill)
                .size(30);

                let content = Column::new()
                    .max_width(800)
                    .spacing(20)
                    .push(title)
                    .push(input)
                    .push(howto);

                Container::new(content)
                    .width(Length::Fill)
                    .center_x()
                    .into()
            }
            Ui::LoadingGuilds(Some(state)) => {
                let title = Text::new("Loading...")
                    .width(Length::Fill)
                    .size(50)
                    .color([0.5, 0.5, 0.5])
                    .horizontal_alignment(HorizontalAlignment::Center);

                Container::new(title).width(Length::Fill).center_x().into()
            }
            Ui::Guilds(Some(state)) => {
                if let Some(guilds) = state.guilds.as_mut() {
                    let mut content = Column::new().spacing(20).push(
                        TextInput::new(&mut state.search, "Search", &*state.search_value, Message::InputChanged)
                            .padding(15)
                            .style(style::Input)
                    );
                    // TODO: Vec of guild with guild having channel field with channel having vec of users?
                    for (gid, g) in guilds {
                        if state.search_ids.len() == 0 || state.search_ids.contains(gid) {
                            content = content.push(Text::new(format!("{}", g.name)));
                            for (vid, v) in &mut g.voices {
                                content = content.push(
                                    Button::new(&mut v.btn_state, Text::new(format!("{}", v.name)))
                                        .on_press(Message::PreviewVoice((gid.clone(), vid.clone())))
                                        .style(style::Button),
                                );

                                for (_, u) in &v.users {
                                    content = content.push(Text::new(format!(
                                        "      {} [{}{}]",
                                        u.name,
                                        if u.deaf { "D" } else { " " },
                                        if u.mute { "M" } else { " " }
                                    )));
                                }
                            }
                            content = content.push(Text::new("    "));
                        }
                    }
                    let scroll = Scrollable::new(&mut state.scroll)
                        .padding(12)
                        .push(Container::new(content))
                        .width(Length::FillPortion(30));

                    let mute_btn = if let Some(in_voice) = &state.in_voice {
                        if state.current_voice.0 == in_voice.0
                            && state.current_voice.1 == in_voice.1
                        {
                            Button::new(&mut state.mute_btn, Text::new(if state.muted { "U" } else { "M" }))
                                .on_press(Message::Mute(in_voice.clone()))
                                .style(style::Button)
                        } else {
                            Button::new(&mut state.mute_btn, Text::new(if state.muted { "U" } else { "M" }))
                                .style(style::Button)
                        }
                    } else {
                        Button::new(&mut state.mute_btn, Text::new(if state.muted { "U" } else { "M" }))
                            .style(style::Button)
                    };

                    let _deaf_btn = if let Some(in_voice) = &state.in_voice {
                        if state.current_voice.0 == in_voice.0
                            && state.current_voice.1 == in_voice.1
                        {
                            Button::new(&mut state.deaf_btn, Text::new("D"))
                                .on_press(Message::Deaf(in_voice.clone()))
                                .style(style::Button)
                        } else {
                            Button::new(&mut state.deaf_btn, Text::new("D"))
                                .style(style::Button)
                        }
                    } else {
                        Button::new(&mut state.deaf_btn, Text::new("D"))
                            .style(style::Button)
                    };

                    let discon_btn = if let Some(in_voice) = &state.in_voice {
                        if state.current_voice.0 == in_voice.0
                            && state.current_voice.1 == in_voice.1
                        {
                            Button::new(&mut state.discon_btn, Text::new("Disconnect"))
                                .on_press(Message::Disconnect(in_voice.clone()))
                                .style(style::Button)
                        } else {
                            Button::new(&mut state.discon_btn, Text::new("Disconnect"))
                                .style(style::Button)
                        }
                    } else {
                        Button::new(&mut state.discon_btn, Text::new("Disconnect"))
                            .style(style::Button)
                    };

                    let grid = Canvas::new(Grid {
                        users: stdMutex::new(&mut state.voice_users),
                        img_cache: stdMutex::new(&mut state.av_cache),
                        grid_state: &mut state.grid_state,
                        current_preview: &state.current_voice,
                        in_voice: &mut state.in_voice,
                        my_user: &mut state.my_user,
                        talking: &state.talking,
                        talk_circ_cache: &state.talk_circ_cache,
                    })
                    .width(Length::Units(GRID_SIZE))
                    .height(Length::Units(GRID_SIZE));

                    let btns = Row::new()
                        .push(mute_btn)
                        //.push(deaf_btn)
                        .push(discon_btn).spacing(8);

                    let gridcol = Column::new()
                        .push(
                            Container::new(Text::new(&state.current_voice.2).size(24))
                                .padding(4)
                                .center_x()
                                .width(Length::Fill),
                        )
                        .push(
                            Container::new(grid)
                                .center_x()
                                .center_y()
                                .width(Length::Fill)
                                .height(Length::Fill),
                        )
                        .push(
                            Container::new(btns)
                                .padding(4)
                                .center_x()
                                .width(Length::Fill),
                        )
                        .width(Length::FillPortion(70))
                        .height(Length::Fill);

                    Row::new().push(scroll).push(gridcol).into()
                } else {
                    Text::new("There's no guilds here yet :(").into()
                }
            }
            _ => Text::new("Oh noes! Please close me and retry!").into(),
        })
        .width(Length::Fill)
        .height(Length::Fill)
        .style(style::Container)
        .into()
    }
}

mod style {
    use iced::text_input::Style;
    use iced::*;

    pub struct Container;

    impl container::StyleSheet for Container {
        fn style(&self) -> container::Style {
            container::Style {
                background: Some(Background::Color(Color::from_rgb8(40, 42, 54))),
                text_color: Some(Color::from_rgb8(182, 183, 186)),
                ..container::Style::default()
            }
        }
    }

    pub struct Button;

    impl button::StyleSheet for Button {
        fn active(&self) -> button::Style {
            button::Style {
                shadow_offset: Default::default(),
                background: Some(Background::Color(Color::from_rgb8(84, 76, 194))),
                border_radius: 4.0,
                border_width: 4.0,
                border_color: Color::from_rgb8(84, 76, 194),
                text_color: Color::from_rgba8(255, 255, 255, 0.75),
            }
        }

        fn hovered(&self) -> button::Style {
            button::Style {
                shadow_offset: Default::default(),
                background: Some(Background::Color(Color::from_rgb8(66, 59, 171))),
                border_radius: 4.0,
                border_width: 4.0,
                border_color: Color::from_rgb8(66, 59, 171),
                text_color: Color::from_rgba8(255, 255, 255, 0.75),
            }
        }
    }

    pub struct Input;

    impl text_input::StyleSheet for Input {
        fn active(&self) -> Style {
            text_input::Style {
                background: Background::Color(Color::from_rgb8(40, 42, 54)),
                border_radius: 6.0,
                border_width: 1.0,
                border_color: Color::from_rgba8(182, 183, 186, 0.234),
            }
        }

        fn focused(&self) -> Style {
            text_input::Style {
                background: Background::Color(Color::from_rgb8(40, 42, 54)),
                border_radius: 6.0,
                border_width: 2.0,
                border_color: Color::from_rgba8(182, 183, 186, 0.234),
            }
        }

        fn placeholder_color(&self) -> Color {
            Color::from_rgba8(182, 183, 186, 0.123)
        }

        fn value_color(&self) -> Color {
            Color::from_rgb8(182, 183, 186)
        }

        fn selection_color(&self) -> Color {
            Color::from_rgb8(189, 147, 249)
        }
    }
}

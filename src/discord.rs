use crate::audioer::AudioAction;
use crate::ui::UiAction;
use audiopus::coder::Encoder;
use audiopus::{Application, Channels, SampleRate};
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use futures::stream::{SplitSink, SplitStream};
use futures::{SinkExt, StreamExt};
use iced::button;
use image::DynamicImage;
use rand::Rng;
use rubato::{InterpolationParameters, InterpolationType, Resampler, SincFixedOut, WindowFunction};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use sodiumoxide::crypto::secretbox::{open, seal, Key, Nonce};
use std::collections::{HashMap, HashSet};
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::str::FromStr;
use std::time::{Duration, Instant};
use thiserror::Error;
use tokio::net::{TcpStream, UdpSocket};
use tokio::sync::mpsc::error::TryRecvError;
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::task::JoinHandle;
use tokio_tungstenite::tungstenite::Message;
use tokio_tungstenite::{tungstenite, MaybeTlsStream, WebSocketStream};

const API_URL: &str = "https://discord.com/api/v9/";
const INTENT: i64 = 1 << 7;

pub fn api(append: &'static str) -> String {
    format!("{}{}", API_URL, append)
}

mod voice_ops {
    pub const IDENTIFY: i64 = 0;
    pub const SELECT_PROTOCOL: i64 = 1;
    pub const READY: i64 = 2;
    pub const HEARTBEAT: i64 = 3;
    pub const DESCRIPTION: i64 = 4;
    pub const SPEAKING: i64 = 5;
    pub const HELLO: i64 = 8;
}

// Opcodes
const GATEWAY_DISPATCH: i64 = 0;
const HEARTBEAT: i64 = 1;
const IDENTIFY: i64 = 2;
const GATEWAY_VOICE_STATE_UPDATE: i64 = 4;
const INVALID_SESSION: i64 = 9;
const HELLO: i64 = 10;
const HEARTBEAT_ACK: i64 = 11;

#[derive(Serialize, Deserialize)]
struct Hello {
    heartbeat_interval: f64,
}

#[derive(Serialize, Deserialize, Debug)]
struct DiscordOp {
    t: Option<String>,
    s: Option<i64>,
    op: i64,
    d: Option<Value>, // todo option for op 9
}

#[derive(Serialize, Deserialize)]
struct Gateway {
    url: String,
}

#[derive(Debug, Clone)]
pub struct User {
    pub avatar: Option<String>,
    pub name: String,
    pub discriminator: String,
    pub id: String,
    pub mute: bool,
    pub deaf: bool,
    pub pos: Option<(u16, u16)>,
    pub av_img: Option<DynamicImage>,
}

#[derive(Debug, Clone)]
pub struct Voice {
    pub name: String,
    // userid, user
    pub users: HashMap<String, User>,
    pub btn_state: button::State,
}

#[derive(Debug, Clone)]
pub struct Guild {
    pub name: String,
    pub voices: HashMap<String, Voice>,
}

pub struct DiscordWebsock {
    start: Instant,
    heartbeat_ms: Option<Duration>,
    voice_heartbeat_ms: Option<Duration>,
    last_hb: Instant,
    last_voice_hb: Instant,
    last_seq: Option<i64>,
    identified: bool,
    ui_tx: Sender<UiAction>,
    action_rx: Receiver<DiscordAction>,
    audio_tx: Sender<AudioAction>,
    guilds: HashMap<String, Guild>,
    ssrc_to_id: HashMap<u32, String>,
    id_to_ssrc: HashMap<String, u32>,
    user: Option<User>,
    token: Option<String>,

    ssrc: Option<u32>,
    voice_secret: Option<Key>,
    voice_addr: Option<SocketAddr>,
    voice_udp: Option<UdpSocket>,
    voice: Option<(String, String)>,
    session_id: Option<String>,
    voice_token: Option<String>,
    voice_url: Option<String>,
    voice_external: Option<(String, u16)>,
    voice_msg_id: u32,
    seq: u16,
    ip_discovery: u8,
    connect_status: u8,

    speaking: u8,
    was_speaking: u8,
    speak_tick: Instant,
    muted: bool,
    deaf: bool,
}

pub enum DiscordAction {
    Token(String),
    /// Guild Id, Voice Id
    Connect((String, String)),
    Disconnect,
    Mute((String, String)),
    Deaf((String, String)),
    /// Guild, Voice, User, us, X, Y, relX, relY
    NewPos((String, String, String, bool, u16, u16, i16, i16)),
}

#[derive(Error, Debug)]
pub enum RunError {
    #[error("Tungstenite({0:?})")]
    Tungstenite(#[from] tungstenite::Error),
    #[error("SerdeJson({0:?})")]
    SerdeJson(#[from] serde_json::Error),
}

impl DiscordWebsock {
    pub fn new() -> (
        Self,
        Receiver<UiAction>,
        Sender<DiscordAction>,
        Receiver<AudioAction>,
    ) {
        let (ui_tx, ui_rx) = channel(10);
        let (action_tx, action_rx) = channel(10);
        let (audio_tx, audio_rx) = channel(10);
        (
            DiscordWebsock {
                start: Instant::now(),
                heartbeat_ms: None,
                voice_heartbeat_ms: None,
                last_hb: Instant::now(),
                last_voice_hb: Instant::now(),
                last_seq: None,
                identified: false,
                ui_tx,
                action_rx,
                audio_tx,
                guilds: HashMap::new(),
                ssrc_to_id: HashMap::new(),
                id_to_ssrc: HashMap::new(),
                user: None,
                token: None,
                ssrc: None,
                voice_secret: None,
                voice_addr: None,
                voice_udp: None,
                voice: None,
                session_id: None,
                voice_token: None,
                voice_url: None,
                voice_external: None,
                voice_msg_id: 0,
                seq: rand::thread_rng().gen(),
                ip_discovery: 0,
                connect_status: 0,

                speaking: 0,
                was_speaking: 0,
                speak_tick: Instant::now(),
                muted: false,
                deaf: false,
            },
            ui_rx,
            action_tx,
            audio_rx,
        )
    }

    fn parse_ready(&mut self, data: &Value) {
        macro_rules! string_or_continue {
            ($val:ident, $get:literal) => {
                if let Some(v) = $val.get($get) {
                    if let Some(v) = v.as_str() {
                        v.to_string()
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }
            };
        }
        macro_rules! bool_or_continue {
            ($val:ident, $get:literal) => {
                if let Some(v) = $val.get($get) {
                    if let Some(v) = v.as_bool() {
                        v
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }
            };
        }
        self.guilds = HashMap::new();
        let user = data.get("user").expect("No user at root");
        let id_val = user.get("id").unwrap();
        let av_val = user.get("avatar").unwrap();
        let dis_val = user.get("discriminator").unwrap();
        let un_val = user.get("username").unwrap();
        self.user = Some(User {
            avatar: av_val.as_str().map(|v| v.to_string()),
            name: un_val.as_str().unwrap().to_string(),
            discriminator: dis_val.as_str().unwrap().to_string(),
            id: id_val.as_str().unwrap().to_string(),
            mute: false,
            deaf: false,
            pos: None,
            av_img: None,
        });
        let guilds = if let Some(guilds) = data.get("guilds") {
            if let Some(guilds) = guilds.as_array() {
                guilds
            } else {
                return;
            }
        } else {
            return;
        };
        for guild in guilds {
            let guild_id = string_or_continue!(guild, "id");
            let guild_name = string_or_continue!(guild, "name");
            let mut store = Guild {
                name: guild_name,
                voices: HashMap::new(),
            };
            let chans = if let Some(v) = guild.get("channels") {
                if let Some(v) = v.as_array() {
                    v
                } else {
                    continue;
                }
            } else {
                continue;
            };

            for chan in chans {
                let ty = if let Some(v) = chan.get("type") {
                    if let Some(v) = v.as_i64() {
                        v
                    } else {
                        continue;
                    }
                } else {
                    continue;
                };

                // voice?
                if ty != 2 {
                    continue;
                }

                let voice_name = string_or_continue!(chan, "name");

                let id = string_or_continue!(chan, "id");

                store.voices.insert(
                    id,
                    Voice {
                        name: voice_name,
                        users: HashMap::new(),
                        btn_state: Default::default(),
                    },
                );
            }

            let voice_states = if let Some(v) = guild.get("voice_states") {
                if let Some(v) = v.as_array() {
                    v
                } else {
                    continue;
                }
            } else {
                continue;
            };

            for state in voice_states {
                let chan_id = string_or_continue!(state, "channel_id");

                let user_id = string_or_continue!(state, "user_id");

                let members = if let Some(v) = guild.get("members") {
                    if let Some(v) = v.as_array() {
                        v
                    } else {
                        continue;
                    }
                } else {
                    continue;
                };

                let mut deaf = bool_or_continue!(state, "deaf");

                let mut mute = bool_or_continue!(state, "mute");

                deaf = deaf || bool_or_continue!(state, "self_deaf");

                mute = mute || bool_or_continue!(state, "self_mute");

                let mut user_store = User {
                    avatar: None,
                    name: "".to_string(),
                    discriminator: "".to_string(),
                    id: user_id.clone(),
                    mute,
                    deaf,
                    pos: None,
                    av_img: None,
                };

                for member in members {
                    let user = if let Some(v) = member.get("user") {
                        v
                    } else {
                        continue;
                    };
                    let mem_id = string_or_continue!(user, "id");

                    if mem_id != user_id {
                        continue;
                    }

                    user_store.avatar = if let Some(v) = user.get("avatar") {
                        v.as_str().map(|v| v.to_string())
                    } else {
                        None
                    };

                    user_store.name = string_or_continue!(user, "username");

                    user_store.discriminator = string_or_continue!(user, "discriminator");
                }

                if let Some(voice) = store.voices.get_mut(&chan_id) {
                    voice.users.insert(user_id, user_store);
                }
            }
            self.guilds.insert(guild_id, store);
        }
        println!("READY PARSED");
        println!("{:?}", self.guilds);
    }

    fn parse_voice_state_update(&mut self, data: &Value) {
        macro_rules! string_or_return {
            ($val:ident, $get:literal) => {
                if let Some(v) = $val.get($get) {
                    if let Some(v) = v.as_str() {
                        v.to_string()
                    } else {
                        println!("failed to get {}", $get);
                        return;
                    }
                } else {
                    println!("failed to get {}", $get);
                    return;
                }
            };
        }
        macro_rules! bool_or_return {
            ($val:ident, $get:literal) => {
                if let Some(v) = $val.get($get) {
                    if let Some(v) = v.as_bool() {
                        v
                    } else {
                        println!("failed to get {}", $get);
                        return;
                    }
                } else {
                    println!("failed to get {}", $get);
                    return;
                }
            };
        }

        let guild = string_or_return!(data, "guild_id");

        let channel = data
            .get("channel_id")
            .map(|v| v.as_str())
            .flatten()
            .map(|v| v.to_string());

        let user_id = string_or_return!(data, "user_id");

        if let (Some((gid, _)), Some(user)) = (&self.voice, &self.user) {
            if &guild == gid && user_id == user.id {
                println!(
                    "SESS {:?}",
                    data.get("session_id")
                        .and_then(|v| v.as_str())
                        .map(|v| v.to_string())
                );
                self.session_id = data
                    .get("session_id")
                    .and_then(|v| v.as_str())
                    .map(|v| v.to_string());
                self.connect_status = 1;
            }
        }

        let member = if let Some(v) = data.get("member") {
            v
        } else {
            return;
        };

        let user = if let Some(v) = member.get("user") {
            v
        } else {
            return;
        };

        let avatar = if let Some(v) = user.get("avatar") {
            v.as_str().map(|v| v.to_string())
        } else {
            None
        };

        let name = string_or_return!(user, "username");

        let discriminator = string_or_return!(user, "discriminator");

        let mut mute = bool_or_return!(member, "mute");

        let mut deaf = bool_or_return!(member, "deaf");

        mute = mute || bool_or_return!(data, "mute");

        deaf = deaf || bool_or_return!(data, "deaf");

        mute = mute || bool_or_return!(data, "self_mute");

        deaf = deaf || bool_or_return!(data, "self_deaf");

        // remove the user from all channels and then add it to the new channel ID if not null
        if let Some(guild_state) = self.guilds.get_mut(&guild) {
            let mut removed = None;
            for (_, voice) in guild_state.voices.iter_mut() {
                if let Some(rem) = voice.users.remove(&user_id) {
                    removed = Some(rem);
                    break;
                }
            }

            if let Some(channel) = channel {
                if let Some(chan) = guild_state.voices.get_mut(&channel) {
                    let (pos, av_img) = if let Some(old) = removed {
                        (old.pos, old.av_img)
                    } else {
                        (None, None)
                    };
                    chan.users.insert(
                        user_id.clone(),
                        User {
                            avatar,
                            name,
                            discriminator,
                            id: user_id,
                            mute,
                            deaf,
                            pos,
                            // Don't want discord mad at anyone for spamming for images we already had
                            av_img,
                        },
                    );
                }
            } else {
                if let Some(ssrc) = self.id_to_ssrc.remove(&user_id) {
                    self.ssrc_to_id.remove(&ssrc);
                }
            }
        }
    }

    async fn handle_voice_op(
        &mut self,
        op: DiscordOp,
        _write: &mut SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, Message>,
    ) {
        // TODO close and resume if no acks
        match op.op {
            voice_ops::HELLO => {
                let hello: Hello =
                    serde_json::from_value(op.d.expect("aaaa").clone()).expect("hello");
                println!("Got vhello: {}", hello.heartbeat_interval);
                self.last_voice_hb = Instant::now();
                self.voice_heartbeat_ms =
                    Some(Duration::from_millis(hello.heartbeat_interval as u64));
            }
            voice_ops::READY => {
                if let Some(opd) = &op.d {
                    self.ssrc = opd.get("ssrc").and_then(|v| v.as_f64()).map(|v| v as u32);
                    if let (Some(ip), Some(port), Some(ssrc)) = (
                        opd.get("ip")
                            .and_then(|v| v.as_str())
                            .map(|v| v.to_string()),
                        opd.get("port").and_then(|v| v.as_f64()).map(|v| v as u16),
                        &self.ssrc,
                    ) {
                        let user = self.user.as_ref().expect("no u what");
                        self.ssrc_to_id.insert(*ssrc, user.id.clone());
                        self.id_to_ssrc.insert(user.id.clone(), *ssrc);

                        let their_addr = SocketAddr::new(
                            IpAddr::V4(Ipv4Addr::from_str(&ip).expect("What")),
                            port,
                        );
                        // TODO: Find a port?
                        let udp = UdpSocket::bind("0.0.0.0:22941").await.expect("Bad conn");
                        udp.connect(&their_addr).await.expect("conn reee");
                        let mut discov = vec![0x0, 0x1, 0x0, 70];
                        discov.extend_from_slice(&ssrc.to_be_bytes());
                        discov.extend_from_slice(&[0; 64]);
                        discov.extend_from_slice(&[0; 2]);
                        udp.send(&discov).await.expect("discov");
                        self.voice_udp = Some(udp);
                        self.voice_addr = Some(their_addr);
                    }
                }
            }
            voice_ops::DESCRIPTION => {
                if let Some(secret) =
                    op.d.expect("aaaa")
                        .get("secret_key")
                        .and_then(|v| v.as_array())
                        .map(|v| {
                            v.iter()
                                .filter_map(|v| v.as_f64().and_then(|v| Some(v as u8)))
                                .collect::<Vec<_>>()
                        })
                {
                    self.voice_secret = Some(Key::from_slice(&secret).expect("key"));
                }
            }
            voice_ops::SPEAKING => {
                println!("asd {:?}", op.d);
                if let Some(opd) = &op.d {
                    if let (Some(user_id), Some(ssrc)) = (
                        opd.get("user_id")
                            .and_then(|v| v.as_str())
                            .map(|v| v.to_string()),
                        opd.get("ssrc").and_then(|v| v.as_f64()).map(|v| v as u32),
                    ) {
                        println!("uadd {}", ssrc);
                        self.ssrc_to_id.insert(ssrc, user_id.clone());
                        self.id_to_ssrc.insert(user_id.clone(), ssrc);
                        //self.ui_tx.send(UiAction::Speaking(speaking > 0, user_id)).await.expect("speak broke");
                    }
                }
            }
            6 => {
                println!("VOICE HB ACK OP WHEEEEEEE");
            }
            opcode => {
                if let Some(d) = &op.d {
                    println!("vo Unknown op {} {:?} {}", opcode, op.t, d.to_string());
                    tokio::fs::write(
                        format!("./vo{}_{}.txt", opcode, op.t.unwrap_or("unk".to_string())),
                        d.to_string(),
                    )
                    .await;
                }
            }
        }
    }

    async fn handle_op(
        &mut self,
        op: DiscordOp,
        write: &mut SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, Message>,
    ) {
        // TODO close and resume if no acks
        match op.op {
            INVALID_SESSION => {
                self.ui_tx.send(UiAction::Relog).await;
            }
            HELLO => {
                let hello: Hello =
                    serde_json::from_value(op.d.expect("aaaa").clone()).expect("hello");
                println!("Got hello: {}", hello.heartbeat_interval);
                self.last_hb = Instant::now();
                self.heartbeat_ms = Some(Duration::from_millis(hello.heartbeat_interval as u64));
            }
            HEARTBEAT => {
                println!("hb {:?}", self.last_seq);
                write
                    .send(Message::Text(
                        json!({
                            "op": HEARTBEAT,
                            "d": self.last_seq,
                        })
                        .to_string(),
                    ))
                    .await
                    .expect("failed send hb");
                self.last_hb = Instant::now();
            }
            GATEWAY_DISPATCH => {
                match op.t.as_ref() {
                    Some(val) => {
                        if val == &"READY".to_string() {
                            self.parse_ready(&op.d.expect("aaaa"));
                            self.ui_tx.send(UiAction::MyUser(self.user.clone())).await;
                            self.ui_tx.send(UiAction::Guilds(self.guilds.clone())).await;
                        } else if val == &"VOICE_STATE_UPDATE".to_string() {
                            // TODO this kills drag
                            self.parse_voice_state_update(&op.d.expect("aaaa"));
                            self.ui_tx.send(UiAction::Guilds(self.guilds.clone())).await;
                        } else if val == &"VOICE_SERVER_UPDATE".to_string() {
                            if let Some(opd) = &op.d {
                                self.voice_token = opd
                                    .get("token")
                                    .and_then(|v| v.as_str())
                                    .map(|v| v.to_string());
                                self.voice_url = opd
                                    .get("endpoint")
                                    .and_then(|v| v.as_str())
                                    .map(|v| v.to_string());
                                self.connect_status = 1;
                            }
                        } else {
                            if let Some(opd) = op.d {
                                println!("Unknown op {} {:?} {}", op.op, op.t, opd.to_string());
                                tokio::fs::write(
                                    format!(
                                        "./{}_{}.txt",
                                        op.op,
                                        op.t.unwrap_or("unk".to_string())
                                    ),
                                    opd.to_string(),
                                )
                                .await;
                            }
                        }
                    }
                    unk => {
                        if let Some(opd) = &op.d {
                            println!("Unknown op {} {:?} {}", op.op, op.t, opd.to_string());
                            tokio::fs::write(
                                format!("./{}_{}.txt", op.op, op.t.unwrap_or("unk".to_string())),
                                opd.to_string(),
                            )
                            .await;
                        }
                    }
                }
            }
            opcode => {
                if let Some(opd) = &op.d {
                    println!("Unknown op {} {:?} {}", opcode, op.t, opd.to_string());
                    tokio::fs::write(
                        format!("./{}_{}.txt", opcode, op.t.unwrap_or("unk".to_string())),
                        opd.to_string(),
                    )
                    .await;
                }
            }
        }
    }

    /// Basically start from the beginning
    pub fn reset(&mut self) {
        self.reset_voice();
        self.token = None;
        self.identified = false;
    }

    fn websock_reader(
        mut read: SplitStream<WebSocketStream<MaybeTlsStream<TcpStream>>>,
        msg_tx: Sender<DiscordOp>,
        err_tx: Sender<Result<(), RunError>>,
    ) -> JoinHandle<()> {
        tokio::spawn(async move {
            while let Some(msg) = read.next().await {
                let msg = match msg {
                    Ok(m) => m,
                    Err(e) => {
                        err_tx.send(Err(RunError::Tungstenite(e))).await;
                        break;
                    }
                };
                let text = match msg.to_text() {
                    Ok(m) => m,
                    Err(e) => {
                        err_tx.send(Err(RunError::Tungstenite(e))).await;
                        break;
                    }
                };
                match serde_json::from_str::<DiscordOp>(text) {
                    Ok(op) => msg_tx.send(op).await.expect("failed to send"),
                    Err(e) => {
                        println!("PARSE ERR OP: {:?}", text);
                        err_tx.send(Err(RunError::SerdeJson(e))).await;
                        break; // TODO? expected value
                    }
                }
            }
            println!("Done reading");
            err_tx.send(Ok(())).await;
        })
    }

    fn reset_voice(&mut self) {
        self.voice = None;
        self.voice_token = None;
        self.voice_url = None;
        self.voice_heartbeat_ms = None;
        self.session_id = None;
        self.connect_status = 0;
        self.ssrc = None;
        self.voice_udp = None;
        self.voice_addr = None;
        self.ip_discovery = 0;
        self.voice_secret = None;
        self.speaking = 0;
        self.was_speaking = 0;
    }

    /// Blocks until dead
    pub async fn run(&mut self) -> Result<(), RunError> {
        loop {
            if let Ok(gateway) = reqwest::get(api("gateway")).await {
                if let Ok(gateway) = gateway.json::<Gateway>().await {
                    let (err_tx, mut err_rx) =
                        tokio::sync::mpsc::channel::<Result<(), RunError>>(128);
                    let (voice_err_tx, mut voice_err_rx) =
                        tokio::sync::mpsc::channel::<Result<(), RunError>>(128);
                    let (msg_tx, mut msg_rx) = tokio::sync::mpsc::channel(128);
                    let (voice_tx, mut voice_rx) = tokio::sync::mpsc::channel(128);
                    let (proc_tx, mut proc_rx) = tokio::sync::mpsc::channel(2048);
                    let (mic_tx, mic_rx) = std::sync::mpsc::channel();
                    let (conn, _) = tokio_tungstenite::connect_async(gateway.url)
                        .await
                        .expect("aaa");
                    let (mut write_main, read_main) = conn.split();
                    let read_main = Self::websock_reader(read_main, msg_tx, err_tx.clone());
                    let mut write_voice = None;
                    let mut read_voice = None;
                    let mut inserted_silence = false;
                    let mut speaking = HashSet::new();

                    Self::start_mic_stream(mic_tx, mic_rx, proc_tx);

                    loop {
                        if self.connect_status == 1 {
                            //println!("REQ: {:?}", (&self.voice_url, &self.session_id, &self.voice_token, &self.voice, &self.user));
                            if let (Some(url), Some(id), Some(token), Some((gid, _)), Some(user)) = (
                                &self.voice_url,
                                &self.session_id,
                                &self.voice_token,
                                &self.voice,
                                &self.user,
                            ) {
                                let (conn, _) =
                                    tokio_tungstenite::connect_async(format!("wss://{}/?v=4", url))
                                        .await
                                        .expect("aaa voice");
                                let (mut w, r) = conn.split();
                                w.send(Message::Text(
                                    json!({
                                        "op": voice_ops::IDENTIFY,
                                        "d": {
                                            "server_id": gid,
                                            "user_id": user.id,
                                            "session_id": id,
                                            "token": token,
                                        },
                                    })
                                    .to_string(),
                                ))
                                .await
                                .expect("failed send voice ident");
                                println!("SENT IDENT FOR VOICE");
                                write_voice = Some(w);
                                read_voice = Some(Self::websock_reader(
                                    r,
                                    voice_tx.clone(),
                                    voice_err_tx.clone(),
                                ));
                                self.connect_status = 2;
                            }
                        }

                        if let (Some(ssrc), Some(user)) = (&self.ssrc, &self.user) {
                            if Instant::now().saturating_duration_since(self.speak_tick)
                                > Duration::from_millis(700)
                            {
                                if self.was_speaking != self.speaking {
                                    if let Some(w) = &mut write_voice {
                                        println!("SENT SPK 2");
                                        self.ui_tx.send(UiAction::Speaking(self.was_speaking > 0, user.id.clone())).await.expect("speak broke");
                                        w.send(Message::Text(
                                            json!({
                                                "op": voice_ops::SPEAKING,
                                                "d": {
                                                    "speaking": self.was_speaking,
                                                    "delay": 0,
                                                    "ssrc": ssrc,
                                                },
                                            })
                                            .to_string(),
                                        ))
                                        .await
                                        .expect("failed send voice speaking 2");
                                        self.speaking = self.was_speaking;
                                    }
                                }
                                self.speak_tick = Instant::now();
                            }
                        }

                        'mic: loop {
                            match proc_rx.try_recv() {
                                Ok(mut v) => {
                                    //println!("CHGOT");
                                    if !self.muted && self.voice_udp.is_some() && self.ip_discovery > 1 {
                                        if self.speaking == 0
                                            && (v.len() != 3
                                                || !(v[0] == 0xF8 && v[1] == 0xFF && v[2] == 0xFE))
                                        {
                                            self.was_speaking = 1;
                                            if !inserted_silence {
                                                v.insert(0, 0xFE);
                                                v.insert(0, 0xFF);
                                                v.insert(0, 0xF8);
                                                inserted_silence = true;
                                            }
                                        } else if self.speaking == 1
                                            && v.len() == 3
                                            && v[0] == 0xF8
                                            && v[1] == 0xFF
                                            && v[2] == 0xFE
                                        {
                                            self.was_speaking = 0;
                                            inserted_silence = false;
                                        }
                                        //println!("Voice {:?}", v);
                                        let packet = self.create_voice_packet(v);
                                        //println!("Packet {:?}", packet);
                                        if let Some(udp) = &self.voice_udp {
                                            //println!("Sned");
                                            udp.send(&packet).await.expect("send voice b");
                                        }
                                    }
                                }
                                Err(TryRecvError::Empty) => {
                                    // sorry nothing
                                    break 'mic;
                                }
                                Err(e) => {
                                    panic!("safsa {:?}", e)
                                }
                            }
                        }

                        // Chances are if we hit this for any reason, we got kicked off the gateway
                        match err_rx.try_recv() {
                            Ok(msg) => {
                                println!("errtx {:?}", msg);
                                self.reset();
                                self.ui_tx
                                    .send(UiAction::Relog)
                                    .await
                                    .expect("Can't send relog 0");
                                break;
                            }
                            Err(TryRecvError::Empty) => {
                                // sorry nothing
                            }
                            Err(e) => {
                                println!("err tx err: {:?}", e);
                                self.reset();
                                self.ui_tx
                                    .send(UiAction::Relog)
                                    .await
                                    .expect("Can't send relog 1");
                                break;
                            }
                        }
                        // Chances are if we hit this for any reason, we got kicked off the gateway
                        match voice_err_rx.try_recv() {
                            Ok(msg) => {
                                println!("voiceerr {:?}", msg);
                                self.reset_voice();
                                if let Some(w) = &mut write_voice {
                                    w.close().await.expect("Bad vws close");
                                }
                                write_voice = None;
                                if let Some(r) = &mut read_voice {
                                    r.await.expect("Disconnect was bad");
                                }
                                read_voice = None;
                                self.ui_tx
                                    .send(UiAction::Disconnected)
                                    .await
                                    .expect("Can't send discon");

                                write_main
                                    .send(Message::Text(
                                        json!({
                                            "op": GATEWAY_VOICE_STATE_UPDATE,
                                            "d": {
                                                "guild_id": Value::Null,
                                                "channel_id": Value::Null,
                                                "self_mute": false,
                                                "self_deaf": false, // TODO
                                            },
                                        })
                                        .to_string(),
                                    ))
                                    .await
                                    .expect("failed send vstu d");
                            }
                            Err(TryRecvError::Empty) => {
                                // sorry nothing
                            }
                            Err(e) => {
                                println!("voice err tx err: {:?}", e);
                                self.reset();
                                self.ui_tx
                                    .send(UiAction::Relog)
                                    .await
                                    .expect("Can't send relog 2");
                                break;
                            }
                        }

                        if let Some(udp) = &self.voice_udp {
                            if self.connect_status == 2 {
                                if let (Some(w), Some(ssrc)) = (&mut write_voice, self.ssrc) {
                                    println!("SENT SPK 1");
                                    w.send(Message::Text(
                                        json!({
                                            "op": voice_ops::SPEAKING,
                                            "d": {
                                                "speaking": 0,
                                                "delay": 0,
                                                "ssrc": ssrc,
                                            },
                                        })
                                        .to_string(),
                                    ))
                                    .await
                                    .expect("failed send voice speaking init");
                                    self.connect_status = 3;
                                }
                            }
                            // 100kb
                            let mut bytes = Vec::with_capacity(100000);
                            match udp.try_recv_buf(&mut bytes) {
                                Ok(n) => {
                                    if self.ip_discovery > 1 {
                                        if let Some(cypher) = &self.voice_secret {
                                            let (nonce, enc_rtp) = bytes[..n].split_at(12);
                                            let mut nonce = nonce.to_vec();
                                            // 24 len nonce based off 12 first bytes
                                            nonce.extend_from_slice(&[0; 12]);
                                            if let Ok(data) = open(
                                                enc_rtp,
                                                &Nonce::from_slice(&nonce).expect("nonce"),
                                                cypher,
                                            ) {
                                                //let seq: u16 = ((nonce[2] as u16) << 8) | nonce[3] as u16;
                                                let ssrc: u32 = ((nonce[8] as u32) << 24)
                                                    | ((nonce[9] as u32) << 16)
                                                    | ((nonce[10] as u32) << 8)
                                                    | (nonce[11] as u32);
                                                if ssrc != self.ssrc.expect("Where's my u32") {
                                                    //println!("{:X?}", data);
                                                    // From discordjs src/client/voice/receiver/PacketHandler.js parseBuffer(buffer)
                                                    if data[0] == 0xBE
                                                        && data[1] == 0xDE
                                                        && data.len() > 4
                                                    {
                                                        let ext_len =
                                                            (data[2] as u16) << 8 | data[3] as u16;
                                                        let mut offset = 4;
                                                        for _ in 0..ext_len {
                                                            let b = data[offset];
                                                            offset += 1;
                                                            if b == 0 {
                                                                continue;
                                                            }
                                                            offset +=
                                                                1 + (0b1111 & (b as usize >> 4));
                                                        }
                                                        // Skip over undocumented Discord byte
                                                        offset += 1;
                                                        // opus data at data[offset..]
                                                        //println!("opus {:X?}", &data[offset..]);
                                                        //println!("VOICEEEEEE {:?}", ssrc);
                                                        if let Some(user) =
                                                            self.ssrc_to_id.get(&ssrc)
                                                        {
                                                            let the_data = data[offset..].to_vec();
                                                            let is_silence = is_sub(&the_data, &[0xF8, 0xFF, 0xFE]);
                                                            self.audio_tx
                                                                .send(AudioAction::Opus(
                                                                    user.clone(),
                                                                    the_data,
                                                                ))
                                                                .await
                                                                .expect("bad atx");
                                                            if is_silence {
                                                                speaking.remove(user);
                                                                self.ui_tx.send(UiAction::Speaking(false, user.clone())).await.expect("speak broke");
                                                            } else if !speaking.contains(user) {
                                                                speaking.insert(user.clone());
                                                                self.ui_tx.send(UiAction::Speaking(true, user.clone())).await.expect("speak broke");
                                                            }
                                                        }
                                                    } else {
                                                        //println!("Weird packet, cont from last? {} {:?}", ssrc, data);
                                                        if let Some(user) =
                                                            self.ssrc_to_id.get(&ssrc)
                                                        {
                                                            self.audio_tx
                                                                .send(AudioAction::Opus(
                                                                    user.clone(),
                                                                    data,
                                                                ))
                                                                .await
                                                                .expect("bad atx");
                                                        }
                                                    }
                                                }
                                            } else {
                                                println!("Failed packet: {:02X?}", bytes);
                                            }
                                        }
                                    } else {
                                        let ty = &bytes[..2];
                                        let uty = (ty[0] as u16) << 8 | (ty[1] as u16);
                                        if uty == 2 {
                                            let _len = &bytes[2..4];
                                            let _ssrc = &bytes[4..8];
                                            let ip_str = &bytes[8..72];
                                            let port = &bytes[72..74];
                                            let nul_range_end = ip_str
                                                .iter()
                                                .position(|&c| c == b'\0')
                                                .unwrap_or(ip_str.len()); // default to length if no `\0` present
                                            let ip =
                                                String::from_utf8(ip_str[..nul_range_end].to_vec())
                                                    .expect("ip str");
                                            let uport = (port[0] as u16) << 8 | (port[1] as u16);
                                            println!("up {} {}", ip, uport);
                                            if let Some(w) = &mut write_voice {
                                                w.send(Message::Text(
                                                    json!({
                                                        "op": voice_ops::SELECT_PROTOCOL,
                                                        "d": {
                                                            "protocol": "udp",
                                                            "data": {
                                                                "address": &ip,
                                                                "port": &uport,
                                                                "mode": "xsalsa20_poly1305",
                                                            }
                                                        },
                                                    })
                                                    .to_string(),
                                                ))
                                                .await
                                                .expect("failed send voice select");
                                            }
                                            self.voice_external = Some((ip, uport));
                                            self.ip_discovery = 2;

                                            let silence =
                                                self.create_voice_packet(vec![0xF8, 0xFF, 0xFE]);
                                            if let Some(udp) = &self.voice_udp {
                                                for i in 0..5 {
                                                    println!("Send silence");
                                                    udp.send(&silence)
                                                        .await
                                                        .expect(&format!("Init sil {}", i));
                                                }
                                            }
                                        } else {
                                            println!("uty is {}", uty);
                                        }
                                    }
                                }
                                Err(e) => {
                                    if e.kind() != std::io::ErrorKind::WouldBlock {
                                        panic!("uuuu {:?}", e);
                                    }
                                }
                            }
                        }

                        match msg_rx.try_recv() {
                            Ok(msg) => {
                                self.last_seq = msg.s;
                                self.handle_op(msg, &mut write_main).await;
                            }
                            Err(TryRecvError::Empty) => {
                                // sorry nothing
                            }
                            Err(e) => {
                                panic!("bbbbb {:?}", e);
                            }
                        }

                        match voice_rx.try_recv() {
                            Ok(msg) => {
                                if let Some(write) = &mut write_voice {
                                    self.handle_voice_op(msg, write).await;
                                }
                            }
                            Err(TryRecvError::Empty) => {
                                // sorry nothing
                            }
                            Err(e) => {
                                panic!("vvvvvvv {:?}", e);
                            }
                        }

                        match self.action_rx.try_recv() {
                            Ok(msg) => {
                                match msg {
                                    DiscordAction::Token(token) => {
                                        self.token = Some(token);
                                    }
                                    DiscordAction::Connect((gid, vid)) => {
                                        write_main
                                            .send(Message::Text(
                                                json!({
                                                    "op": GATEWAY_VOICE_STATE_UPDATE,
                                                    "d": {
                                                        "guild_id": &gid,
                                                        "channel_id": &vid,
                                                        "self_mute": false,
                                                        "self_deaf": false,
                                                    },
                                                })
                                                .to_string(),
                                            ))
                                            .await
                                            .expect("failed send vstu c");
                                        self.voice = Some((gid, vid));
                                    }
                                    DiscordAction::NewPos((gid, vid, user, us, x, y, relx, rely)) => {
                                        println!("Got new pos");
                                        if let Some(g) = self.guilds.get_mut(&gid) {
                                            if let Some(v) = g.voices.get_mut(&vid) {
                                                if let Some(u) = v.users.get_mut(&user) {
                                                    println!("Ins new pos");
                                                    u.pos = Some((x, y));
                                                }
                                            }
                                        }
                                        self.audio_tx
                                            .send(AudioAction::Moved(user, us, (x, y), (relx, rely)))
                                            .await
                                            .expect("mv");
                                    }
                                    DiscordAction::Disconnect => {
                                        println!("Disconn");
                                        self.reset_voice();
                                        if let Some(w) = &mut write_voice {
                                            w.close().await.expect("Bad vws close");
                                        }
                                        write_voice = None;
                                        if let Some(r) = &mut read_voice {
                                            r.await.expect("Disconnect was bad");
                                        }
                                        read_voice = None;
                                    }
                                    DiscordAction::Mute((_gid, _vid)) => {
                                        self.muted = !self.muted;
                                    }
                                    DiscordAction::Deaf((_gid, _vid)) => {
                                        self.deaf = !self.deaf;
                                    }
                                }
                            }
                            Err(TryRecvError::Empty) => {
                                // sorry nothing
                            }
                            Err(e) => {
                                panic!("bbbbb {:?}", e);
                            }
                        }

                        if let Some(write) = &mut write_voice {
                            if let Some(time) = self.voice_heartbeat_ms.as_ref() {
                                if &Instant::now().saturating_duration_since(self.last_voice_hb)
                                    >= time
                                {
                                    println!("SEND VHB");
                                    let n: u32 = rand::thread_rng().gen();
                                    write
                                        .send(Message::Text(
                                            json!({
                                                "op": voice_ops::HEARTBEAT,
                                                "d": n,
                                            })
                                            .to_string(),
                                        ))
                                        .await
                                        .expect("failed send vhb");
                                    self.last_voice_hb = Instant::now();
                                }
                            }
                        }
                        if let Some(time) = self.heartbeat_ms.as_ref() {
                            if !self.identified {
                                if let Some(token) = self.token.as_ref() {
                                    self.identified = true;
                                    println!("ident");
                                    // TODO: os
                                    write_main
                                        .send(Message::Text(
                                            json!({
                                                "op": IDENTIFY,
                                                "d": {
                                                    "token": token,
                                                    "properties": {
                                                        "$os": "windows",
                                                        "$browser": "zaxord",
                                                        "$device": "zaxord",
                                                    },
                                                    "intents": INTENT,
                                                },
                                            })
                                            .to_string(),
                                        ))
                                        .await
                                        .expect("failed send ident");
                                }
                            }
                            if &Instant::now().saturating_duration_since(self.last_hb) >= time {
                                println!("hb {:?}", self.last_seq);
                                write_main
                                    .send(Message::Text(
                                        json!({
                                            "op": HEARTBEAT,
                                            "d": self.last_seq,
                                        })
                                        .to_string(),
                                    ))
                                    .await
                                    .expect("failed send hb");
                                self.last_hb = Instant::now();
                            }
                        }
                        tokio::time::sleep(Duration::from_millis(1)).await;
                    }
                    if read_voice.is_some() {
                        read_voice.unwrap().await.expect("Failed read");
                    }
                    read_main.await.expect("Failed read");
                    tokio::time::sleep(Duration::from_millis(5000)).await;
                }
            }
        }
    }

    fn start_mic_stream(
        tx: std::sync::mpsc::Sender<Vec<f32>>,
        rx: std::sync::mpsc::Receiver<Vec<f32>>,
        proc_tx: Sender<Vec<u8>>,
    ) {
        let host = cpal::default_host();
        let device = host
            .default_input_device()
            .expect("no input device available");
        let config = device
            .default_input_config()
            .expect("No config good enough");
        let chans = config.channels();
        let rate = config.sample_rate().0;

        if chans > 2 {
            panic!("Your mic has too many channels bro");
        }

        let err_fn = move |err| {
            panic!("an error occurred on stream: {}", err);
        };

        println!(
            "{:?} C {} R {} {:?}",
            device.name(),
            chans,
            rate,
            config.sample_format()
        );

        tokio::spawn(async move {
            let encoder = Encoder::new(SampleRate::Hz48000, Channels::Mono, Application::Voip)
                .expect("bad opus enc");
            let mut resampler = SincFixedOut::<f32>::new(
                48000.0 / rate as f64,
                InterpolationParameters {
                    sinc_len: 256,
                    f_cutoff: 0.95,
                    interpolation: InterpolationType::Linear,
                    oversampling_factor: 128,
                    window: WindowFunction::BlackmanHarris2,
                },
                48 * 20,
                chans as usize,
            );
            let mut expected = resampler.nbr_frames_needed();
            let mut chunk = vec![Vec::with_capacity(expected), Vec::with_capacity(expected)];
            let mut silence = 0;
            let mut thresh_avg = 0.0;
            let mut thresh_below = 0u8;
            let mut last_below = false;
            loop {
                match rx.try_recv() {
                    Ok(data) => {
                        //println!("PROCESS");

                        for (ind, s) in data.iter().enumerate() {
                            // uninterleave
                            if chans == 2 {
                                //println!("PS 2");
                                chunk[ind % 2].push(*s);
                            } else {
                                //println!("PSc {}", chans);
                                chunk[0].push(*s);
                            }
                            thresh_avg += s.abs();
                            //println!("CH {} {}", chunk[0].len(), expected);
                            if chunk[0].len() == expected
                                && (chans == 1 || chunk[1].len() == expected)
                            {
                                //println!("CHUNK {}", chunk[0].len() + chunk[1].len());
                                if thresh_avg / (expected as f32) < 0.0141 {
                                    thresh_avg = 0.0;
                                    thresh_below += 1;
                                    if thresh_below > 16 {
                                        chunk = vec![
                                            Vec::with_capacity(expected),
                                            Vec::with_capacity(expected),
                                        ];
                                        if !last_below {
                                            last_below = true;
                                            let proc_tx = proc_tx.clone();
                                            tokio::spawn(async move {
                                                proc_tx.send(vec![0xF8, 0xFF, 0xFE]).await.ok();
                                                proc_tx.send(vec![0xF8, 0xFF, 0xFE]).await.ok();
                                                proc_tx.send(vec![0xF8, 0xFF, 0xFE]).await.ok();
                                                proc_tx.send(vec![0xF8, 0xFF, 0xFE]).await.ok();
                                                proc_tx.send(vec![0xF8, 0xFF, 0xFE]).await.ok();
                                            });
                                        }
                                        continue;
                                    }
                                } else {
                                    thresh_below = 0;
                                    last_below = false;
                                }

                                // resample, opus encode, send
                                let waves_out = resampler.process(&chunk).unwrap();
                                //println!("waves {} {:?}", waves_out[0].len() + waves_out[1].len(), waves_out);
                                chunk[0].clear();
                                chunk[1].clear();
                                // reinterleave to 2 chan
                                let mut inter = Vec::with_capacity(expected * 2);
                                if chans == 1 {
                                    for v in &waves_out[0] {
                                        inter.push(v.clone());
                                        inter.push(*v);
                                    }
                                } else {
                                    /*for i in 0..waves_out[0].len() * 2 {
                                        inter.push(waves_out[i % 2][i / 2]);
                                    }*/
                                    for i in 0..waves_out[0].len() {
                                        inter.push(waves_out[0][i] + waves_out[1][i] * 0.5);
                                    }
                                }

                                //println!("i {} {:?}", inter.len(), inter);
                                let mut result = vec![0; 3024];
                                let num = encoder.encode_float(&inter, &mut result).expect("encf");

                                //println!("n {}", num);

                                if num > 0 {
                                    silence = 0;
                                    let proc_tx = proc_tx.clone();
                                    tokio::spawn(async move {
                                        if let Err(e) = proc_tx.send(result[..num].to_vec()).await {
                                            std::process::exit(1);
                                        }
                                    });
                                } else if silence < 5 {
                                    silence += 1;
                                    let proc_tx = proc_tx.clone();
                                    tokio::spawn(async move {
                                        proc_tx.send(vec![0xF8, 0xFF, 0xFE]).await.ok();
                                    });
                                }
                                thresh_avg = 0.0;
                                expected = resampler.nbr_frames_needed();
                                chunk = vec![
                                    Vec::with_capacity(expected),
                                    Vec::with_capacity(expected),
                                ];
                            }
                        }
                    }
                    Err(std::sync::mpsc::TryRecvError::Empty) => {}
                    Err(e) => {
                        panic!("mic rxe {:?}", e);
                    }
                }
            }
        });

        std::thread::spawn(move || {
            let stream = match config.sample_format() {
                cpal::SampleFormat::F32 => device.build_input_stream(
                    &config.into(),
                    move |data: &[f32], _: &cpal::InputCallbackInfo| {
                        write_input_data::<f32>(data, tx.clone())
                    },
                    err_fn,
                ),
                cpal::SampleFormat::I16 => device.build_input_stream(
                    &config.into(),
                    move |data: &[i16], _: &cpal::InputCallbackInfo| {
                        write_input_data::<i16>(data, tx.clone())
                    },
                    err_fn,
                ),
                cpal::SampleFormat::U16 => device.build_input_stream(
                    &config.into(),
                    move |data: &[u16], _: &cpal::InputCallbackInfo| {
                        write_input_data::<u16>(data, tx.clone())
                    },
                    err_fn,
                ),
            }
            .expect("Bad input stream");
            loop {
                stream.play().expect("Bad play");
            }
        });
    }

    fn create_voice_packet(&mut self, opus: Vec<u8>) -> Vec<u8> {
        let mut data = Vec::with_capacity(1024);
        data.extend_from_slice(&[0x80u8, 0x78]);
        self.seq = self.seq.wrapping_add(1);
        data.extend(self.seq.to_be_bytes());
        self.voice_msg_id = self.voice_msg_id.wrapping_add(960);
        data.extend(self.voice_msg_id.to_be_bytes());
        data.extend(self.ssrc.as_ref().unwrap().to_be_bytes());
        if let Some(cypher) = &self.voice_secret {
            let mut nonce = data.clone();
            nonce.extend_from_slice(&[0; 12]);
            //println!("Secreted audio {:?} {:?} {:?}", cypher, nonce, opus);
            data.extend_from_slice(&seal(
                &opus,
                &Nonce::from_slice(&nonce).expect("nonce"),
                cypher,
            ));
        }
        data
    }
}

fn write_input_data<T>(data: &[T], tx: std::sync::mpsc::Sender<Vec<f32>>)
where
    T: cpal::Sample,
{
    let tx = tx.clone();
    let proc: Vec<f32> = data
        .iter()
        .map(|i| {
            let s: f32 = cpal::Sample::from(i);
            s
        })
        .collect();
    tx.send(proc).expect("greebl");
}

fn is_sub<T: PartialEq>(mut haystack: &[T], needle: &[T]) -> bool {
    if needle.len() == 0 { return true; }
    while !haystack.is_empty() {
        if haystack.starts_with(needle) { return true; }
        haystack = &haystack[1..];
    }
    false
}